import setuptools

setuptools.setup(
    name='cecatracker',
    version='0.0.1',
    Author='Ulf Liebal',
    author_email='ulf.liebal@rwth-aachen.de',
    description='The cecatracker simulates central carbon metabolite masses in isotopic labeling experiments.',
    url='https://git.rwth-aachen.de/ulf.liebal/cecatracker.git',
    install_requires=[
        'numpy>=1.18.0',
        'recordclass>=0.14.0'
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT',
        'Operating System :: OS Independent'
    ],
    python_requires='>=3.6',
    packages=['cecatracker'],
#     package_dir={'': 'src'},
)
