###########################Imports######################################
import numpy as np
import os
# import urllib.request
import tkinter as tk
import itertools
#import keyboard
import re
import ast
# from tkinter import ttk 
import tkinter.scrolledtext as scrolledtext
from .cctCode import *
########################################################################
# cecatracker generates labeling patterns for central carbon cycle.
# Input is the pyruvate labeling and different active reactions can be
# choosed. 
# In addition the fragments mass increase in mdvs data can be prospected. 
########################################################################

# https://stackoverflow.com/questions/35612235/how-to-read-numpy-2d-array-from-string #3
def cecatrackerGUI():

    def callback_confirm():
    # 	print('Click!')
        tracer = str2array(tracer_entry.get())
        strategy = str2array(strategy_entry.get())

        incct = (np.array([[0,0,0,0]]), tracer, strategy)
        outcct = cecatracker(incct)

    # 	print(str(outcct))

        str_cit, str_oga, str_suc, str_oaa = outcctmdv02str(outcct)
        text_output_cit.configure(state = tk.NORMAL)
        text_output_cit.delete('1.0', tk.END)
        text_output_cit.insert(tk.END, str_cit)
        text_output_cit.configure(state = tk.DISABLED)

        text_output_oga.configure(state = tk.NORMAL)
        text_output_oga.delete('1.0', tk.END)
        text_output_oga.insert(tk.END, str_oga)
        text_output_oga.configure(state = tk.DISABLED)

        text_output_suc.configure(state = tk.NORMAL)
        text_output_suc.delete('1.0', tk.END)
        text_output_suc.insert(tk.END, str_suc)
        text_output_suc.configure(state = tk.DISABLED)

        text_output_oaa.configure(state = tk.NORMAL)
        text_output_oaa.delete('1.0', tk.END)
        text_output_oaa.insert(tk.END, str_oaa)
        text_output_oaa.configure(state = tk.DISABLED)

        text_output_mdv.configure(state = tk.NORMAL)
        text_output_mdv.delete('1.0', tk.END)
        text_output_mdv.insert(tk.END, outcctmdv12str(outcct))
        text_output_mdv.configure(state = tk.DISABLED)

    class list_callback_handler:
        infos = ['Below are the entries Tracer and Strategy. To use the generation insert ' + '\n'
        + 'one or more tracers in the form [[1,0,1]] or [[1,0,1],[1,1,1]]. ¹³C-isotpes are marked with one.' + '\n'
        + 'The same way you can choose strategies. Here is an example with chars instead of the numbers [[a,b,c]].' + '\n'
        + 'The letters stands for TCA cycle (a), glyoxylat shunt (b) and PPc (c). Use 1 for active and 0 for inactive.'+ '\n' +
        'For glucose as sole carbon source notice that it goes into the glycolysis. So two pyruvates are remaining.' + '\n' +
        'The first three from glucose are inverted and the last three stay in their positions.']

        x_infos = 0


        def callback_pre_infos(self):
            self.x_infos -= 1

            if self.x_infos < 0:
                self.x_infos = len(self.infos)-1

            label1.config(text=self.infos[self.x_infos])


        def callback_nex_infos(self):
            self.x_infos += 1

            if self.x_infos >= len(self.infos):
                self.x_infos = 0

            label1.config(text=self.infos[self.x_infos])

        def callback_pre_tasks(self):
            self.x_tasks -= 1
            if self.x_tasks < 0:
                self.x_tasks = len(self.tasks)-1

            label2.config(text=self.tasks[self.x_tasks])

        def callback_nex_tasks(self):
            self.x_tasks += 1
            if self.x_tasks >= len(self.tasks):
                self.x_tasks = 0
            label2.config(text=self.tasks[self.x_tasks])




    ########################Main Window#####################################
    ########################################################################



    root = tk.Tk()
    root.title('cecatracker')
    root.geometry('1300x800')

    text_output_cit = scrolledtext.ScrolledText(root, height=15, width=16)
    text_output_cit.place(x=515,y=365)
    text_output_oga = scrolledtext.ScrolledText(root, height=15, width=14)
    text_output_oga.place(x=665,y=365)
    text_output_suc = scrolledtext.ScrolledText(root, height=15, width=12)
    text_output_suc.place(x=800,y=365)
    text_output_oaa = scrolledtext.ScrolledText(root, height=15, width=12)
    text_output_oaa.place(x=920,y=365)
    text_output_mdv = scrolledtext.ScrolledText(root, height=15, width=25)
    text_output_mdv.place(x=1040,y=365)

    ########################Widgets#########################################

    callback_handler = list_callback_handler()

    #button_exc_pre = tk.Button(root, text='previous', command=callback_handler.callback_pre_infos)# command noch implementieren
    # afterwards use variants of string infos
    label1 = tk.Label(root, text=callback_handler.infos[0], bg='white', fg='black')
    #button_nex_aft = tk.Button(root, text='next', command=callback_handler.callback_nex_infos)# command noch implementieren
    #button_exc_pre2 = tk.Button(root, text='previous', command=callback_handler.callback_pre_tasks)# command noch implementieren
    # afterwards use variants of string tasks
    #label2 = tk.Label(root, text=callback_handler.tasks[0], bg='lightblue', fg='black')
    #button_nex_aft2 = tk.Button(root, text='next', command=callback_handler.callback_nex_tasks)# command noch implementieren
#     MetFile = 'https://git.rwth-aachen.de/ulf.liebal/CarbonTracker/-/raw/master/Figure/model1a.png' 
    myMetFig = os.path.join('images','model1a.png')

    pic = tk.PhotoImage(file=myMetFig)
    label3 = tk.Label(root, image = pic)
    label4 = tk.Label(root)####
    label5 = tk.Label(root, text='Tracer: ', bg='white')
    tracer_entry = tk.Entry(root, bd=5, width=40)
    label6 = tk.Label(root, text='Strategy: ', bg='white')
    strategy_entry = tk.Entry(root, bd=5, width=40)
    button_confirm = tk.Button(root, text='confirm', command=callback_confirm)

    ####################FramePosition#######################################
    #button_exc_pre.place(x=35,y=35, width=80,height=40)
    label1.place(x=230,y=10, width=820,height=150)
    #button_nex_aft.place(x=1005,y=35, width=80,height=40)
    #button_exc_pre2.place(x=35,y=135, width=80,height=40)
    #label2.place(x=150,y=120, width=820,height=100)
    #button_nex_aft2.place(x=1005,y=135, width=80,height=40)
    label3.place(x=20,y=280)
    label4.place(x=655,y=216, width=658,height=100)
    label5.place(x=655,y=220)
    tracer_entry.place(x=740,y=215)
    label6.place(x=655,y=265)
    strategy_entry.place(x=740,y=260)
    button_confirm.place(x=840,y=300)



    ###############Main Loop################################################
    root.mainloop()

    
def str2array(s):
    # Remove space after [
    s=re.sub('\[ +', '[', s.strip())
    # Replace commas and spaces
    s=re.sub('[,\s]+', ', ', s)
    return(np.array(ast.literal_eval(s)))

def outcctmdv02str(outcct):
    retstr_cit = 'CIT:\n [s,r,q,c,b,p]\n' + str(outcct[0].CIT)
    retstr_oga = 'OGA:\n [s,r,q,c,b]\n' + str(outcct[0].OGA)
    retstr_suc = 'SUC:\n [r,q,c,b]\n [b,c,q,r]\n [p,q,c,b]\n [b,c,q,p]\n' + str(outcct[0].SUC)
    retstr_oaa = 'OAA:\n [p,q,r,s]\n' + str(outcct[0].OAA) #  [s,r,c,b]\n [s,r,c,b]\n [p,q,c,b]\n [b,c,q,p]\n

    return retstr_cit, retstr_oga, retstr_suc, retstr_oaa

    #retstr = 'CIT465: ' + str(1) + '\t\tOGA465: ' + str(2)
def outcctmdv12str(outcct):
    retstr = ''
    retstr += 'CIT465:\t M+ ' + str(outcct[1].CIT465) + '\n'
    retstr += 'OGA465:\t M+' + str(outcct[1].OGA465) + '\n'
    retstr += 'SUC247:\t M+' + str(outcct[1].SUC247) + '\n'
    retstr += 'FUM245:\t M+' + str(outcct[1].FUM245) + '\n'
    retstr += 'MAL233:\t M+' + str(outcct[1].MAL233) + '\n'
    retstr += 'MAL245:\t M+' + str(outcct[1].MAL245) + '\n'
    retstr += 'ASP334:\t M+' + str(outcct[1].ASP334) + '\n'

    retstr += 'GLU302:\t M+' + str(outcct[1].GLU302) + '\n'
    retstr += 'GLU330:\t M+' + str(outcct[1].GLU330) + '\n'
    retstr += 'GLU404:\t M+' + str(outcct[1].GLU404) + '\n'
    retstr += 'GLU432:\t M+' + str(outcct[1].GLU432) + '\n'
    retstr += 'GLU474:\t M+' + str(outcct[1].GLU474) + '\n'

    retstr += 'PRO184:\t M+' + str(outcct[1].PRO184) + '\n'
    retstr += 'PRO286:\t M+' + str(outcct[1].PRO286) + '\n'
    retstr += 'PRO328:\t M+' + str(outcct[1].PRO328) + '\n'

    retstr += 'ASP302:\t M+' + str(outcct[1].ASP302) + '\n'
    retstr += 'ASP316:\t M+' + str(outcct[1].ASP316) + '\n'
    retstr += 'ASP390:\t M+' + str(outcct[1].ASP390) + '\n'
    retstr += 'ASP418:\t M+' + str(outcct[1].ASP418) + '\n'
    retstr += 'ASP460:\t M+' + str(outcct[1].ASP460) + '\n'

    retstr += 'THR376:\t M+' + str(outcct[1].THR376) + '\n'
    retstr += 'THR404:\t M+' + str(outcct[1].THR404) + '\n'
    retstr += 'THR446:\t M+' + str(outcct[1].THR446)

    return retstr
