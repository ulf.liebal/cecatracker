####################################
# Imports
####################################
import numpy as np
import itertools
from recordclass import recordclass

####################################
# NamedTuple
####################################
metabolite_type_mdv0 = recordclass('metabolite_mdv0', 'CIT OGA SUC OAA')
metabolite_type_mdv1 = recordclass('metabolite_mdv1', 'CIT465 OGA465 SUC247 FUM245 MAL233 MAL245 ASP334 GLU302 GLU330 GLU404 GLU432 GLU474 PRO184 PRO286 PRO328 ASP302 ASP316 ASP390 ASP418 ASP460 THR376 THR404 THR446')

####################################
# Functions
####################################

def core(oaa, pyr):
	'''
	Core TCA reaction common to all pathways. The input can be composed of multiple label patterns. All pattern combinations are generated.
	'''
	ace = np.unique(np.flip(pyr[:,1:]), axis=0)
	# https://stackoverflow.com/questions/1953194/permutations-of-two-lists-in-python
	my_perms = list(itertools.product(range(len(ace)), range(len(oaa))))
	cit = np.unique(np.vstack([np.concatenate([np.flip(oaa)[np.array(r)[1],:3],ace[np.array(r)[0],:],np.flip(oaa)[np.array(r)[1],3:]]) for r in my_perms]), axis=0)
	oga = np.unique(cit[:,:-1], axis=0)
	return cit, oga


def tca(oga):
	oaa = np.unique(np.vstack([oga[:,1:], np.flip(oga[:,1:])]), axis=0)
	return oaa

def glx(cit, pyr):
	suc = np.unique(np.hstack([cit[:,-1:],cit[:,2:-1]]), axis=0)
	suc = np.unique(np.vstack([suc, np.flip(suc)]), axis=0)
	# for the glyoxylate arm we have to consider all permutations among the patterns of pyruvate and citrate (first two atoms)
	cit12 = np.unique(cit[:,:2], axis=0)
	ace = np.unique(np.flip(pyr[:,1:]), axis=0)
	my_perms = list(itertools.product(range(len(ace)), range(len(cit12))))
	mal = np.unique(np.vstack([np.concatenate([cit12[np.array(r)[1],:],ace[np.array(r)[0],:]]) for r in my_perms]), axis=0)
	mal = np.unique(np.vstack([mal]), axis=0)
	oaa = np.unique(np.vstack([suc, mal]), axis=0)
	return suc, oaa

def ppc(pyr):
	oaapre = np.hstack((pyr,np.zeros((pyr.shape[0],1), dtype=int)))
	oaa = np.unique(np.vstack([oaapre]), axis=0)
	return oaa

def frag_summe (fragment):
	frag = fragment
	frag_sum = np.unique(np.sum([frag], axis = 2))
	#print (frag_sum)
	return frag_sum


####################################
# Main Function- cecatracker
####################################

# incct = (np.array([[0,0,0,0]]), np.array([[0,1,0]]), np.array([[0,1,0]]))


def cecatracker(incct):
	oaa, pyr, strategy = incct
	#pyr = np.array([[0,1,0]]) #[0,0,1],
	#oaa = np.array([[0,0,0,0]]) #,[0,1,0,0],[0,0,1,0]
	
	outcct = (metabolite_type_mdv0('','','',''), metabolite_type_mdv1('','','','','','','','','','','','','','','','','','','','','','',''))
	
	mystrat = ('Strategy_{}'.format(strategy))
	for item in strategy:
		TCA=bool(item[0])
		GLX=bool(item[1])
		PPC=bool(item[2])
		loop = 1
		oaa_old = np.empty(1)
		oaa_init = np.array([[0,0,0,0]])
		
		while not np.array_equal(oaa_init, oaa_old):
# 			print('loop:{}'.format(loop))
			oaa_old = oaa_init
			cit, oga = core(oaa, pyr)
			if PPC:
				oaa = np.unique(np.vstack((oaa_old, ppc(pyr))), axis=0)
				if TCA and not GLX:
# 					print('TCA')
					oaa = np.unique(np.vstack((tca(oga),ppc(pyr))), axis = 0)
					suc = oaa
					#print('OAA')
# 					print(oaa)
				elif GLX and not TCA:
					suc, oaa_tmp = glx(cit, pyr)
					oaa = np.unique(np.vstack((oaa_tmp,ppc(pyr))), axis = 0)
				elif TCA and GLX:
					suc, oaa_tmp = glx(cit, pyr)
					oaa = np.unique(np.vstack((oaa_tmp,tca(oga),ppc(pyr))), axis = 0)
			else:
				oaa = oaa_old
			if TCA and not GLX and not PPC:
				oaa = tca(oga)
				suc = oaa
# 				print(oaa)
			elif GLX and not TCA and not PPC:
				suc, oaa = glx(cit, pyr)
# 				print(oaa)
			elif TCA and GLX and not PPC:
				suc, oaa_tmp = glx(cit, pyr)
				oaa = np.unique(np.vstack((tca(oga), oaa_tmp)), axis=0)
# 				print(oaa)
			oaa_init = oaa
			loop += 1
			
		outcct[0].OGA = oga
		outcct[0].CIT = cit
		outcct[0].SUC = suc
		outcct[0].OAA = oaa
		
# 		print('oxaloacetate: {}'.format(len(oaa)))
		# mdv generation [1 2] means m+1 and m+2 labeling for e.g.
		# function need mal and suc no fum cause fum equals suc
		if np.array_equal(oaa_init, oaa_old):
			CIT465 = frag_summe(cit)
			OGA465 = frag_summe(oga)
			SUC247 = frag_summe(suc)
			FUM245 = frag_summe(suc)
			MAL233 = frag_summe(oaa[:,1:])
			MAL245 = frag_summe(oaa)
			ASP334 = frag_summe(oaa)
			GLU302 = frag_summe(oga[:,:2])
			GLU330 = frag_summe(oga[:,1:])
			GLU404 = frag_summe(oga[:,1:])
			GLU432 = frag_summe(oga)
			GLU474 = frag_summe(oga)
			PRO184 = frag_summe(oga[:,1:])
			PRO286 = frag_summe(oga)
			PRO328 = frag_summe(oga)
			ASP302 = frag_summe(oaa[:,:2])
			ASP316 = frag_summe(oaa[:,1:])
			ASP390 = frag_summe(oaa[:,1:])
			ASP418 = frag_summe(oaa)
			ASP460 = frag_summe(oaa)
			THR376 = frag_summe(oaa[:,1:])
			THR404 = frag_summe(oaa)
			THR446 = frag_summe(oaa)
			outcct[1].CIT465 = CIT465
			outcct[1].OGA465 = OGA465
			outcct[1].SUC247 = SUC247
			outcct[1].FUM245 = FUM245
			outcct[1].MAL233 = MAL233
			outcct[1].MAL245 = MAL245
			outcct[1].ASP334 = ASP334
			outcct[1].GLU302 = GLU302
			outcct[1].GLU330 = GLU330
			outcct[1].GLU404 = GLU404
			outcct[1].GLU432 = GLU432
			outcct[1].GLU474 = GLU474
			outcct[1].PRO184 = PRO184
			outcct[1].PRO286 = PRO286
			outcct[1].PRO328 = PRO328
			outcct[1].ASP302 = ASP302
			outcct[1].ASP316 = ASP316
			outcct[1].ASP390 = ASP390
			outcct[1].ASP418 = ASP418
			outcct[1].ASP460 = ASP460
			outcct[1].THR376 = THR376
			outcct[1].THR404 = THR404
			outcct[1].THR446 = THR446
			return outcct
        
